package eu.letsplayonline.telegram.bungee;

import eu.letsplayonline.telegram.core.api.Player;
import eu.letsplayonline.telegram.core.api.TelegramInterface;
import eu.letsplayonline.telegram.core.api.TimeoutException;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.UUID;

/**
 * Created by julian on 21.04.15.
 */
public class PanicCommand extends Command {
    private TelegramInterface telegram = TMMain.telegram;

    public PanicCommand() {
        super("helpop", "telegram.panic", "emergency");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (sender instanceof ProxiedPlayer) {
            if (args != null && args.length > 0) {
                try {
                    final StringBuilder builder = new StringBuilder();
                    for (String arg : args) {
                        builder.append(arg).append(" ");
                    }
                    telegram.sendRequest(new Player(((ProxiedPlayer) sender).getDisplayName(), ((ProxiedPlayer) sender).getUniqueId()), builder.toString());
                } catch (TimeoutException e) {
                    TextComponent emsg = new TextComponent("Ooops, something went wrong!");
                    emsg.setColor(ChatColor.RED);
                    sender.sendMessage(emsg);
                }
                TextComponent msg = new TextComponent("Message successfully sent!");
                msg.setColor(ChatColor.GREEN);
                sender.sendMessage(msg);
            } else {
                TextComponent msg = new TextComponent("Correct usage>: /panic <message>");
                msg.setColor(ChatColor.RED);
                sender.sendMessage(msg);
            }
        } else {
            try {
                telegram.sendRequest(new Player("Console", UUID.randomUUID()), "Looks like console wants to test stuff ;)");
            } catch (TimeoutException e) {
                e.printStackTrace();
            }
        }
    }
}
