package eu.letsplayonline.telegram.bungee;

import eu.letsplayonline.telegram.core.api.TelegramInterface;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 * Created by Julian Fölsch on 31.10.15.
 *
 * @author Julian Fölsch
 */
public class ShutdownCommand extends Command {
    private TelegramInterface telegram = TMMain.telegram;

    public ShutdownCommand() {
        super("shutdown", "telegram.shutdown");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (sender instanceof ProxiedPlayer) {
            if (sender.hasPermission("telegram.shutdown")) {
                final StringBuilder builder = new StringBuilder();
                for (String arg : args) {
                    builder.append(arg).append(" ");
                }
                TMMain.reason = builder.toString();
                TMMain.proxy.stop(TMMain.reason);
            }
        }else{
            final StringBuilder builder = new StringBuilder();
            for (String arg : args) {
                builder.append(arg).append(" ");
            }
            TMMain.reason = builder.toString();
            TMMain.proxy.stop(TMMain.reason);
        }
    }
}