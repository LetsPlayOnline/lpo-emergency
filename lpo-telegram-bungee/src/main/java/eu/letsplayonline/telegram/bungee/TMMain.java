package eu.letsplayonline.telegram.bungee;

import eu.letsplayonline.telegram.core.Telegram;
import eu.letsplayonline.telegram.core.api.Player;
import eu.letsplayonline.telegram.core.api.TelegramInterface;
import eu.letsplayonline.telegram.core.api.TimeoutException;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;

import java.io.IOException;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by julian on 20.04.15.
 */
public class TMMain extends Plugin {
    public static TelegramInterface telegram = null;
    public static Logger logger = ProxyServer.getInstance().getLogger();
    public static ProxyServer proxy = ProxyServer.getInstance();
    public static String reason = "Not planned!";
    public void onEnable() {
        telegram = Telegram.getInstance();
        telegram.addCommandListener(new TelegramCommandListener());
        getProxy().getScheduler().runAsync(this, new Runnable() {
            @Override
            public void run() {
                try {
                    telegram.initialize("LPO-Bungee", "Telegram-Notifier", "localhost", 6785, 120); //todo get from config
                } catch (IOException e) {
                    logger.log(Level.SEVERE, String.valueOf(e.getStackTrace()));
                }
                try {
                    telegram.sendRequest(new Player("Status", UUID.randomUUID()), "I have started!");
                } catch (TimeoutException e) {
                    logger.log(Level.SEVERE, String.valueOf(e.getStackTrace()));
                }
            }
        });
        getProxy().getPluginManager().registerCommand(this, new PanicCommand());
        getProxy().getPluginManager().registerCommand(this, new ShutdownCommand());
    }

    public void onDisable() {
        getProxy().getPluginManager().unregisterCommands(this);
        try {
            telegram.sendRequest(new Player("Status", UUID.randomUUID()), "Stopping with the following reason: " + reason);
        } catch (TimeoutException e) {
            logger.log(Level.SEVERE, String.valueOf(e.getStackTrace()));
        }
        telegram.shutdown();
    }
}
