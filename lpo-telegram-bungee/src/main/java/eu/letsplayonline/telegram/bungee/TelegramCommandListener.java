package eu.letsplayonline.telegram.bungee;

import eu.letsplayonline.telegram.core.api.CommandListener;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;

public class TelegramCommandListener implements CommandListener {
    private static ProxyServer proxy = TMMain.proxy;

	@Override
	public void sendMessage(String message) {
		ProxyServer.getInstance().broadcast(new TextComponent(message));
	}

	@Override
	public void sendPM(String player, String message) {
        TextComponent msg = new TextComponent("You got an message from an admin! Answer it with /helpop <msg>");
        msg.setColor(ChatColor.DARK_RED);
        TextComponent msg2 = new TextComponent(message);
        msg2.setColor(ChatColor.DARK_RED);
        if (proxy.getPlayer(player) != null) {
            proxy.getPlayer(player).sendMessage(msg);
            proxy.getPlayer(player).sendMessage(msg2);
        }
    }

	@Override
	public void kickPlayer(String player, String reason) {
		// TODO Auto-generated method stub

	}

	@Override
	public void banPlayer(String player, String reason, long duration) {
		// TODO Auto-generated method stub

	}

}
