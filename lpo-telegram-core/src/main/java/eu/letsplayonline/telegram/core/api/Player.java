package eu.letsplayonline.telegram.core.api;

import java.util.UUID;

public class Player {

	private final String name;
	private final UUID uuid;

	public Player(String theName, UUID theUniqueId) {
		this.name = theName;
		this.uuid = theUniqueId;
	}

	public UUID getUUID() {
		return uuid;
	}

	public String getName() {
		return name;
	}
}
