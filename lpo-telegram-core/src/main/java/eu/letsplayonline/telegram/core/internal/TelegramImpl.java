package eu.letsplayonline.telegram.core.internal;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import eu.letsplayonline.telegram.core.api.CommandListener;
import eu.letsplayonline.telegram.core.api.Player;
import eu.letsplayonline.telegram.core.api.TelegramInterface;
import eu.letsplayonline.telegram.core.api.TimeoutException;

public class TelegramImpl implements TelegramInterface, Runnable {

	private String chatName = "ServerBot";
	private CommandListener cl = null;
	private String serverName = "LPO-Bungee";
	private int messageTimeout = 60;
	private String prefix = null;
	private volatile boolean running = false;
	private Map<UUID, Long> playerCooldowns = new ConcurrentHashMap<UUID, Long>();
	private Map<String, UUID> nameIdMappings = new ConcurrentHashMap<String, UUID>();
	BufferedReader reader = null;
	BufferedWriter writer = null;
	Socket sock = null;

	/**
	 * {@inheritDoc}
	 */
	public void sendRequest(Player source, String message)
			throws TimeoutException {
		handlePlayerTimeout(source);
		// send actual request
		try {
			sendMessage(source.getName() + " " + message);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public void addCommandListener(CommandListener listener) {
		cl = listener;
	}

	/**
	 * {@inheritDoc}
	 */
	public void initialize(String serverName, String chatName, String host,
			int port, int messageTimeout) throws IOException {

		if (running) {
			throw new IllegalStateException(
					"Already running! you have to stop the running instance first!");
		}
		try {
			sock = new Socket(host, port);
			this.writer = new BufferedWriter(new OutputStreamWriter(
					sock.getOutputStream()));
			writer.write("main_session\n");
			writer.write("dialog_list\n");
			writer.flush();
			this.reader = new BufferedReader(new InputStreamReader(
					sock.getInputStream()));

		} catch (UnknownHostException e) {
			throw new IOException(e);
		}

		this.serverName = serverName;
		this.chatName = chatName;
		this.messageTimeout = messageTimeout;

		prefix = "msg " + chatName + " [LPO-Telegram-Bot] " + serverName + " ";
		running = true;
		new Thread(this).start();
	}

	/**
	 * {@inheritDoc}
	 */
	public void shutdown() {
		running = false;

	}

	/**
	 * {@inheritDoc}
	 */
	public void run() {

		while (running) {
			try {
				if (reader.ready()) {
					String line = reader.readLine();
					// System.out.println(line);
					if (line != null
							&& line.startsWith("[")
							&& line.contains(chatName)
							&& (line.contains("@" + serverName) || line
									.contains("PING"))) {
						if (line.contains("PING")) {
							sendMessage("I'm here!");
							continue;
						}
						line = line.substring(
								line.indexOf(serverName) + serverName.length())
								.trim();
						String[] msgArray = line.split(" ");
						try {
							Protocol keyword = Protocol.valueOf(msgArray[0]
									.toUpperCase());

							switch (keyword) {
							case MSG:
								cl.sendMessage(line.substring(line.indexOf(" ") + 1));
								break;

							case BAN:
								/**
								 * 1 - target 2 - duration 3 - reason
								 */
								String target = msgArray[1];
								String duration = msgArray[2];
								String reason = line.substring(
										line.indexOf(duration)
												+ duration.length()).trim();
								cl.banPlayer(target, reason,
										Long.parseLong(duration));
								break;

							case KICK:
								target = msgArray[1];
								reason = line.substring(
										line.indexOf(target) + target.length())
										.trim();
								cl.kickPlayer(target, reason);
								break;

							case PM:
								target = msgArray[1];
								String message = line.substring(
										line.indexOf(target) + target.length())
										.trim();
								cl.sendPM(target, message);
								break;
							case RESET:
								resetPlayerTimeout(line.substring(
										line.indexOf(" ") + 1).trim());
								break;

							case HELP:
								showHelp();
								break;

							default:
								break;
							}
						} catch (IllegalArgumentException e) {
							sendMessage("Sorry, invalid command given! Valid commands are (case-insensitive) "
									+ Protocol.list());
						}
					}
				} else {
					Thread.sleep(1000);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		try {
			if (writer != null) {
				writer.close();
			}
		} catch (IOException e2) {
			e2.printStackTrace();
		}
		try {
			if (reader != null) {
				reader.close();
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		try {
			if (sock != null) {
				sock.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Shows the command help for the bot.
	 */
	private void showHelp() {
		synchronized (this) {
			try {
				sendMessage("==============================");
				Thread.sleep(200);
				sendMessage("Help for the LPO telegram bot");
				Thread.sleep(200);
				sendMessage(" ");
				Thread.sleep(200);
				sendMessage("General syntax: @<bot name> <command> <arg1> <arg2> etc.");
				Thread.sleep(200);
				sendMessage(" - help  -  shows this help.");
				Thread.sleep(200);
				sendMessage(" - msg <message>  -  displays a public message.");
				Thread.sleep(200);
				sendMessage(" - pm <target> <message>  -  Sends a private message to the given player.");
				Thread.sleep(200);
				sendMessage(" - kick <target> <reason>  -  kicks a player with the given reason.");
				Thread.sleep(200);
				sendMessage(" - ban <target> <duration> <reason>  -  bans a player with the given reason for the given duration (in seconds!).");
				Thread.sleep(200);
				sendMessage(" - reset <target>  -  resets the cooldown of the given player so that he can answer again.");
				Thread.sleep(200);
				sendMessage("==============================");

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Sends a message via telegram to the targeted chat
	 * 
	 * @param message
	 *            - the message to send.
	 * @throws IOException
	 *             - when communication to the Telegram-Cli is broken.
	 */
	private void sendMessage(String message) throws IOException {
		synchronized (this) {
			writer.write(prefix + message + "\n");
			writer.flush();
		}
	}

	/**
	 * Resets the timeout a player has to wait until he can send another
	 * request.
	 * 
	 * @param namepart
	 *            - part of the name.
	 * @throws IOException
	 *             - when communication to the Telegram-Cli is broken.
	 */
	private void resetPlayerTimeout(String namepart) throws IOException {
		boolean found = false;
		for (Entry<String, UUID> entry : nameIdMappings.entrySet()) {
			if (entry.getKey().contains(namepart.toLowerCase())) {
				found = true;
				sendMessage("Timeout for player " + entry.getKey()
						+ " removed.");
				playerCooldowns.remove(entry.getValue());
				break;
			}
		}
		if (!found) {
			sendMessage("Could not find player " + namepart + "!");
		}
	}

	/**
	 * Handles the player timeout management.
	 * 
	 * @param source
	 *            - the player to check for timeout.
	 * @throws TimeoutException
	 *             - when the player is not allowed to write a request.
	 */
	private void handlePlayerTimeout(Player source) throws TimeoutException {
		if (!playerCooldowns.containsKey(source.getUUID())) {
			playerCooldowns.put(source.getUUID(), System.currentTimeMillis()
					+ messageTimeout * 1000);
		} else {
			long cooldown = playerCooldowns.get(source.getUUID())
					- System.currentTimeMillis();
			if (cooldown > 0) {
				throw new TimeoutException(
						"You cannot send another request for " + cooldown
								/ 1000 + " seconds!");
			} else {
				playerCooldowns.put(source.getUUID(),
						System.currentTimeMillis() + messageTimeout * 1000);
			}
		}

		nameIdMappings.put(source.getName().toLowerCase(), source.getUUID());
	}
}
