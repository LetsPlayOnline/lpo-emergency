package eu.letsplayonline.telegram.core.internal;

public enum Protocol {
	KICK, BAN, MSG, PM, RESET, HELP;

	public static String list() {
		StringBuilder sb = new StringBuilder();
		for (Protocol value : Protocol.values()) {
			sb.append(value.toString().toLowerCase()).append(",");
		}
		return sb.toString().substring(0, sb.toString().length() - 1);
	}
}
