package eu.letsplayonline.telegram.core.api;

public interface CommandListener {

	/**
	 * Send a public message to the server.
	 * 
	 * @param message
	 *            - the message to send.
	 */
	void sendMessage(String message);

	/**
	 * Send a private message to a given player.
	 * 
	 * @param player
	 *            - the player to send the message to.
	 * @param message
	 *            - the message.
	 */
	void sendPM(String player, String message);

	/**
	 * Kicks a player based on his name. Can also be a partial name.
	 * 
	 * @param player
	 *            - the player to kick
	 * @param reason
	 *            - the reason for the kick.
	 */
	void kickPlayer(String player, String reason);

	/**
	 * Bans a player based on his Name. Can also be a partial name.
	 * 
	 * @param player
	 *            - the player to ban.
	 * @param reason
	 *            - the reason for the ban.
	 * @param duration
	 *            - the duration, in seconds.
	 */
	void banPlayer(String player, String reason, long duration);

}
