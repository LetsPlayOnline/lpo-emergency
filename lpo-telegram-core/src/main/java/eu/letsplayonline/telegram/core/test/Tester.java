package eu.letsplayonline.telegram.core.test;

import java.io.IOException;
import java.util.UUID;

import eu.letsplayonline.telegram.core.Telegram;
import eu.letsplayonline.telegram.core.api.CommandListener;
import eu.letsplayonline.telegram.core.api.Player;
import eu.letsplayonline.telegram.core.api.TelegramInterface;
import eu.letsplayonline.telegram.core.api.TimeoutException;

public class Tester {

	public static void main(String[] args) throws IOException, TimeoutException {

		TelegramInterface telegram = Telegram.getInstance();
		telegram.addCommandListener(new CommandListener() {

			public void sendPM(String player, String message) {
				System.out.println("PM: " + player + " - " + message);
			}

			public void sendMessage(String message) {
				System.out.println("Message: " + message);
			}

			public void kickPlayer(String player, String reason) {
				System.out.println("Kick: " + player + " - " + reason);
			}

			public void banPlayer(String player, String reason, long duration) {
				System.out.println("Ban: " + player + " - " + reason + " - "
						+ duration);
			}
		});
		telegram.initialize("Gummibox", "1234", "localhost", 12345, 60);

		telegram.sendRequest(new Player("Gummihuhn", UUID.randomUUID()),
				"TELEGRAM CORE TESTMESSAGE");
	}
}
