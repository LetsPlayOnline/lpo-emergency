package eu.letsplayonline.telegram.core.api;

import java.io.IOException;

public interface TelegramInterface {

	void sendRequest(Player source, String message) throws TimeoutException;

	void addCommandListener(CommandListener listener);

	void initialize(String serverName, String chatName, String host, int port,
			int messageTimeout) throws IOException;

	void shutdown();

}
