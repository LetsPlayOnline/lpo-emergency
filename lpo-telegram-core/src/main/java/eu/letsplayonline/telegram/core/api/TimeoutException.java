package eu.letsplayonline.telegram.core.api;

public class TimeoutException extends Exception {

	public TimeoutException(String message) {
		super(message);
	}
}
