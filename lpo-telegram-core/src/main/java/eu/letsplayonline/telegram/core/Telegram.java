package eu.letsplayonline.telegram.core;

import eu.letsplayonline.telegram.core.api.TelegramInterface;
import eu.letsplayonline.telegram.core.internal.TelegramImpl;

public class Telegram {

	private Telegram() {

	}

	public static TelegramInterface getInstance() {
		return new TelegramImpl();
	}
}
